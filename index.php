<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Гейне Генрих Павлович 211-321</title>
  <link rel="stylesheet" href="/style/index.css">
</head>

<body>
  <header>
    <img class="logo" src="/images/polytech-logo.png" alt="Логотип">
    <h1 class="title">Лабораторные по PHP</h1>
  </header>

  <main>
    <?php
      $hosts = ['http://php-labs.std-1716.ist.mospolytech.ru/'];
      $gits = ['https://gitlab.com/GeGePa/php-labs'];

      for ($i = 0; $i < count($hosts); $i++) {
        $number = $i + 1;
        echo "
          <div class='card'>
            <h2 class='card__text'>Лабораторная работа № $number</h2>
            <div class='card__links'>
              <a class='card__link' href='$hosts[$i]'>Сайт</a>
              <a class='card__link' href='$gits[$i]'>GitLab</a>
            </div>
          </div>
        ";
      }
    ?>
  </main>

  <footer>
    <p class="footer__text">Гейне Генрих Павлович 211-321</p>
  </footer>
</body>

</html>